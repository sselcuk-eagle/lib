# sselcuk Eagle Library

Device libraries match IEC 81346 (RefDes) and IEC 60617 (Symbols)

## installation

- Download repository

```
# using SSH protocol
git clone git@gitlab.com:sselcuk-eagle/lib.git
```

- Add repository to library paths on eagle.
  - Run eagle
  - Under control panel "Options>Directories"
  - Add colon (:) as delimiter and repository path to libraries field.
  
  ```
  :<repository path>
  ```
